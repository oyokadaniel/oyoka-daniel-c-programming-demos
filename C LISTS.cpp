#include <stdio.h>
#include <stdlib.h>

struct Node
{
    int number;
    struct Node *next;
};

struct Node *createNode(int num);
void printList(struct Node *head);
void append(struct Node **head, int num);
void prepend(struct Node **head, int num);
void deleteByKey(struct Node **head, int key);
void deleteByValue(struct Node **head, int value);
void insertAfterKey(struct Node **head, int key, int value);
void insertAfterValue(struct Node **head, int searchValue, int newValue);

int main()
{
    struct Node *head = NULL;
    int choice, data, key, value;

    while (1)
    {
        printf("\nLinked Lists\n");
        printf("1. Print List\n");
        printf("2. Append\n");
        printf("3. Prepend\n");
        printf("4. Delete by Key\n");
        printf("5. Delete by Value\n");
        printf("6. Insert After Key\n");
        printf("7. Insert After Value\n");
        printf("8. Exit\n");
        printf("Enter your choice: ");
        scanf("%d", &choice);

        switch (choice)
        {
        case 1:
            printf("List Of Prototypes: ");
            printList(head);
            break;
        case 2:
            printf("Enter data to append: ");
            scanf("%d", &data);
            append(&head, data);
            printf("Data appended successfully.\n");
            break;
        case 3:
            printf("Enter data to prepend: ");
            scanf("%d", &data);
            prepend(&head, data);
            printf("Data prepended successfully.\n");
            break;
        case 4:
            printf("Enter key to delete: ");
            scanf("%d", &key);
            deleteByKey(&head, key);
            printf("Node with key %d deleted successfully.\n", key);
            break;
        case 5:
            printf("Enter value to delete: ");
            scanf("%d", &value);
            deleteByValue(&head, value);
            printf("Node with value %d deleted successfully.\n", value);
            break;
        case 6:
            printf("Enter key after which to insert: ");
            scanf("%d", &key);
            printf("Enter value to insert: ");
            scanf("%d", &value);
            insertAfterKey(&head, key, value);
            printf("Data inserted successfully after key %d.\n", key);
            break;
        case 7:
            printf("Enter value after which to insert: ");
            scanf("%d", &value);
            printf("Enter new value to insert: ");
            scanf("%d", &data);
            insertAfterValue(&head, value, data);
            printf("Data inserted successfully after value %d.\n", value);
            break;
        case 8:
            printf("Exiting...\n");
            exit(0);
        default:
            printf("Invalid choice. Please try again.\n");
        }
    }

    return 0;
}

//create a new node with given data
struct Node *createNode(int num)
{
    struct Node *newNode = (struct Node *)malloc(sizeof(struct Node));
    if (newNode == NULL)
    {  
        printf("Error: Unable to allocate memory for a new node\n");
        exit(1);
    }

    newNode->number = num;
    newNode->next = NULL;
    return newNode;
}

//print all nodes in the list
void printList(struct Node *head)
{
    struct Node *current = head;
    printf("[");
    while (current != NULL)
    {
        printf("%d", current->number);
        current = current->next;
        if (current != NULL)
        {
            printf(", ");
        }
    }
    printf("]\n");
}

// add a node at the end of the list
void append(struct Node **head, int num)
{
    struct Node *newNode = createNode(num);
    if (*head == NULL)
    {
        *head = newNode;
        return;
    }
    struct Node *current = *head;
    while (current->next != NULL)
    {
        current = current->next;
    }
    current->next = newNode;
}

//add a node at the beginning of the list
void prepend(struct Node **head, int num)
{
    struct Node *newNode = createNode(num);
    newNode->next = *head;
    *head = newNode;
}

//delete a node by key
void deleteByKey(struct Node **head, int key)
{
    struct Node *temp = *head, *prev = NULL;

    if (temp != NULL && temp->number == key)
    {
        *head = temp->next;
        free(temp);
        return;
    }

    while (temp != NULL && temp->number != key)
    {
        prev = temp;
        temp = temp->next;
    }

    if (temp == NULL)
        return;

    prev->next = temp->next;
    free(temp);
}

//delete a node by value
void deleteByValue(struct Node **head, int value)
{
    struct Node *temp = *head, *prev = NULL;

    if (temp != NULL && temp->number == value)
    {
        *head = temp->next;
        free(temp);
        return;
    }

    while (temp != NULL && temp->number != value)
    {
        prev = temp;
        temp = temp->next;
    }

    if (temp == NULL)
        return;

    prev->next = temp->next;
    free(temp);
}

//insert a node after a key
void insertAfterKey(struct Node **head, int key, int value)
{
    struct Node *temp = *head;
    while (temp != NULL && temp->number != key)
    {
        temp = temp->next;
    }
    if (temp == NULL)
    {
        printf("Key not found in the list.\n");
        return;
    }
    struct Node *newNode = createNode(value);
    newNode->next = temp->next;
    temp->next = newNode;
}

//insert a node after a value
void insertAfterValue(struct Node **head, int searchValue, int newValue)
{
    struct Node *temp = *head;
    while (temp != NULL && temp->number != searchValue)
    {
        temp = temp->next;
    }
    if (temp == NULL)
    {
        printf("Value not found in the list.\n");
        return;
    }
    struct Node *newNode = createNode(newValue);
    newNode->next = temp->next;
    temp->next = newNode;
}
